package com.fernandokahan.imaginecurve

import android.support.test.rule.ActivityTestRule

import org.junit.Rule
import org.junit.Test

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.espresso.matcher.ViewMatchers.withText
import org.hamcrest.Matchers.containsString


class MainActivityTest {

    @Rule @JvmField
    var activityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun setEditText1Test() {
        setEditTextTest(R.id.editText1, 3)
    }

    @Test
    fun setEditText2Test() {
        setEditTextTest(R.id.editText2, 3)
    }

    @Test
    fun setEditText3Test() {
        setEditTextTest(R.id.editText3, 3)
    }

    @Test
    fun setEditText4Test() {
        setEditTextTest(R.id.editText4, 3)
    }

    @Test
    fun setEditText5Test() {
        setEditTextTest(R.id.editText5, 3)
    }

    @Test
    fun setEditText6Test() {
        setEditTextTest(R.id.editText6, 3)
    }

    @Test
    fun setEditTextTwiceTest() {
        val viewId = R.id.editText1;
        setEditTextTest(viewId, 3)
        onView(withId(viewId))
                .perform(click())
                .perform(clearText())
        setEditTextTest(viewId, 7)
    }

    private fun setEditTextTest(viewId: Int, value: Int) {
        val valueStr = value.toString()
        onView(withId(viewId))
                .perform(click())
                .perform(typeText(valueStr))

        onView(withId(R.id.textViewSum1))
                .check(matches(withText(containsString(valueStr))))
    }
}