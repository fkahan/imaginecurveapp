package com.fernandokahan.imaginecurve

import android.app.Application
import org.junit.Test
import org.junit.Before

class TheViewModelUnitTests {

    private lateinit var vm: TheViewModel

    @Before
    fun setUp() {
        vm = TheViewModel(Application())
    }

    @Test
    fun sum_whenVal1IsSet_sumIsCorrect() {

        // Arrange
        val value = 20L

        // Act
        vm.setVal1(value);

        // Assert
        vm.getSum().test().assertValue { t -> t == value }
    }

    @Test
    fun sum_whenVal2IsSet_sumIsCorrect() {

        // Arrange
        val value = 20L;

        // Act
        vm.setVal2(value);

        // Assert
        vm.getSum().test().assertValue { t -> t == value }
    }

    @Test
    fun sum_whenVal3IsSet_sumIsCorrect() {

        // Arrange
        val value = 20L;

        // Act
        vm.setVal3(value);

        // Assert
        vm.getSum().test().assertValue { t -> t == value }
    }

    @Test
    fun sum_whenVal4IsSet_sumIsCorrect() {

        // Arrange
        val value = 20L;

        // Act
        vm.setVal4(value);

        // Assert
        vm.getSum().test().assertValue { t -> t == value }
    }

    @Test
    fun sum_whenVal5IsSet_sumIsCorrect() {

        // Arrange
        val value = 20L;

        // Act
        vm.setVal5(value);

        // Assert
        vm.getSum().test().assertValue { t -> t == value }
    }

    @Test
    fun sum_whenVal6IsSet_sumIsCorrect() {

        // Arrange
        val value = 20L;

        // Act
        vm.setVal6(value);

        // Assert
        vm.getSum().test().assertValue { t -> t == value }
    }

    @Test
    fun sum_whenAllValuesAreSet_sumIsCorrect() {

        // Arrange
        val value = 20L;
        val total = value * 6; // 6 fields

        // Act
        vm.setVal1(value);
        vm.setVal2(value);
        vm.setVal3(value);
        vm.setVal4(value);
        vm.setVal5(value);
        vm.setVal6(value);

        // Assert
        vm.getSum().test().assertValue { t -> t == total }
    }
}
