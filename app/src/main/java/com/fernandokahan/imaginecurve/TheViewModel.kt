package com.fernandokahan.imaginecurve

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.disposables.CompositeDisposable

class TheViewModel(context: Application) : AndroidViewModel(context) {

    private var disposable = CompositeDisposable()

    private var sum: BehaviorSubject<Long> = BehaviorSubject.create()
    private var val1:BehaviorSubject<Long> = BehaviorSubject.create()
    private var val2:BehaviorSubject<Long> = BehaviorSubject.create()
    private var val3:BehaviorSubject<Long> = BehaviorSubject.create()
    private var val4:BehaviorSubject<Long> = BehaviorSubject.create()
    private var val5:BehaviorSubject<Long> = BehaviorSubject.create()
    private var val6:BehaviorSubject<Long> = BehaviorSubject.create()

    var isFlashingWithRxJava = false
    var isFlashingWithTimer = false

    init {
        val1.onNext(0)
        val2.onNext(0)
        val3.onNext(0)
        val4.onNext(0)
        val5.onNext(0)
        val6.onNext(0)

        disposable.add(val1.subscribe({ updateSum() }))
        disposable.add(val2.subscribe({ updateSum() }))
        disposable.add(val3.subscribe({ updateSum() }))
        disposable.add(val4.subscribe({ updateSum() }))
        disposable.add(val5.subscribe({ updateSum() }))
        disposable.add(val6.subscribe({ updateSum() }))
    }

    fun setVal1(num: Long) {
        val1.onNext(num)
    }

    fun setVal2(num: Long) {
        val2.onNext(num)
    }

    fun setVal3(num: Long) {
        val3.onNext(num)
    }

    fun setVal4(num: Long) {
        val4.onNext(num)
    }

    fun setVal5(num: Long) {
        val5.onNext(num)
    }

    fun setVal6(num: Long) {
        val6.onNext(num)
    }

    fun updateSum() {
        val n1 = val1.value
        val n2 = val2.value
        val n3 = val3.value
        val n4 = val4.value
        val n5 = val5.value
        val n6 = val6.value
        val total = n1 + n2 + n3 + n4 + n5 + n6

        sum.onNext(total)
    }

    fun getSum() : Observable<Long>{
        return sum
    }
}
