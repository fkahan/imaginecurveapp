package com.fernandokahan.imaginecurve

import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.content_main.*
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.inputmethod.InputMethodManager
import com.fernandokahan.imaginecurve.tasks.FlashingTaskRxJava
import com.fernandokahan.imaginecurve.tasks.FlashingTaskTimer
import com.fernandokahan.imaginecurve.tasks.ITask


class MainActivityPresenter(private val activity: MainActivity, private val viewModel: TheViewModel) : Disposable {

    private val flashingInterval = 500L
    private var disposable = CompositeDisposable()

    private val flashingTask1: ITask
    private val flashingTask2: ITask

    init {
        flashingTask1 = FlashingTaskRxJava(activity, activity.textViewSum1, flashingInterval)
        flashingTask2 = FlashingTaskTimer(activity, activity.textViewSum2, flashingInterval)

        startStopFlashing(flashingTask1, viewModel.isFlashingWithRxJava)
        startStopFlashing(flashingTask2, viewModel.isFlashingWithTimer)
    }

    fun setObservablesAndListeners() {

        disposable.add(
                RxTextView.textChanges(activity.editText1).subscribe({ t ->
                    viewModel.setVal1(if (t.isNullOrEmpty()) 0 else t.toString().toLong())
                })
        )

        disposable.add(
                RxTextView.textChanges(activity.editText2).subscribe({ t ->
                    viewModel.setVal2(if (t.isNullOrEmpty()) 0 else t.toString().toLong())
                })
        )

        disposable.add(
                RxTextView.textChanges(activity.editText3).subscribe({ t ->
                    viewModel.setVal3(if (t.isNullOrEmpty()) 0 else t.toString().toLong())
                })
        )

        disposable.add(
                RxTextView.textChanges(activity.editText4).subscribe({ t ->
                    viewModel.setVal4(if (t.isNullOrEmpty()) 0 else t.toString().toLong())
                })
        )

        disposable.add(
                RxTextView.textChanges(activity.editText5).subscribe({ t ->
                    viewModel.setVal5(if (t.isNullOrEmpty()) 0 else t.toString().toLong())
                })
        )

        disposable.add(
                RxTextView.textChanges(activity.editText6).subscribe({ t ->
                    viewModel.setVal6(if (t.isNullOrEmpty()) 0 else t.toString().toLong())
                })
        )

        disposable.add(
                RxView.clicks(activity.textViewSum1).subscribe({
                    clearFocus()
                    viewModel.isFlashingWithRxJava = !viewModel.isFlashingWithRxJava
                    startStopFlashing(flashingTask1, viewModel.isFlashingWithRxJava)
                })
        )

        disposable.add(
                viewModel.getSum().subscribe({ t ->
                    val text = t.toString()
                    activity.textViewSum1.text = text
                    activity.textViewSum2.text = text
                })
        )

        activity.textViewSum2.setOnClickListener {
            viewModel.isFlashingWithTimer = !viewModel.isFlashingWithTimer
            startStopFlashing(flashingTask2, viewModel.isFlashingWithTimer)
        }
    }

    private fun startStopFlashing(flashingTask: ITask, isFlashing: Boolean) {
        if (isFlashing) {
            flashingTask.start()
        } else {
            flashingTask.stop()
        }
    }

    // Removes the focus from the EditText fields
    private fun clearFocus() {
        activity.layout.requestFocus()
        val view = activity.currentFocus
        if (view != null) {
            val imm = activity.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    override fun isDisposed(): Boolean {
        return disposable.isDisposed
    }

    override fun dispose() {
        if (!disposable.isDisposed)
            disposable.dispose()
    }
}