package com.fernandokahan.imaginecurve.tasks

import android.app.Activity
import android.graphics.Color
import android.widget.TextView

import com.fernandokahan.imaginecurve.MainActivity
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.content_main.*

import java.util.concurrent.TimeUnit

class FlashingTaskRxJava(private val activity: Activity, private val textView: TextView, private val flashingInterval: Long) : ITask {

    private var isFlashing = false
    private var flashingObservable: Disposable? = null

    override fun start() {
        flashingObservable = Observable.interval(flashingInterval, TimeUnit.MILLISECONDS)
                .timeInterval()
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    isFlashing = !isFlashing
                    textView.setTextColor(if (isFlashing) Color.BLACK else Color.TRANSPARENT)
                })
    }

    override fun stop() {
        textView.setTextColor(Color.BLACK)
        flashingObservable?.dispose()
    }
}
