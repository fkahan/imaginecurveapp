package com.fernandokahan.imaginecurve.tasks

interface ITask {
    fun start()
    fun stop()
}
