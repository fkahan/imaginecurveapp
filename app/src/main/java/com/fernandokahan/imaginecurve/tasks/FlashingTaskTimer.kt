package com.fernandokahan.imaginecurve.tasks

import android.app.Activity
import android.graphics.Color
import android.widget.TextView
import java.util.Timer
import java.util.TimerTask

class FlashingTaskTimer(private val activity: Activity, private val textView: TextView, private val flashingInterval: Long) : ITask {
    private var timer: Timer? = null
    private var isFlashing: Boolean = false

    init {
        this.timer = Timer()
    }

    override fun start() {
        timer!!.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                isFlashing = !isFlashing
                textView.setTextColor(if (isFlashing) Color.BLACK else Color.TRANSPARENT)
            }
        }, 0, flashingInterval)
    }

    override fun stop() {
        activity.runOnUiThread { textView.setTextColor(Color.BLACK) }

        timer!!.cancel()
        timer = Timer()
    }
}
